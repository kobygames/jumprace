﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[ExecuteInEditMode]
public class Bezier : MonoBehaviour
{

	public Vector3[] points;
	 public LevelDetails leveldetail;

	int index = 0, count;


	private void Start()
		{
#if !UNITY_EDITOR
		Renderer[] renderers = GetComponentsInChildren<Renderer>();

		for (int i = 0; i < renderers.Length; i++)
			{
			renderers[i].enabled = false;
			}

#endif
#if UNITY_EDITOR
		Renderer[] renderers = GetComponentsInChildren<Renderer>();

		for (int i = 0; i < renderers.Length; i++)
			{
			//renderers[i].enabled = true;
			}

#endif
		}



	private void OnDrawGizmos()
		{

		//if (Application.isPlaying) //don't runt update in playing mode
		//	return;
		count = gameObject.transform.childCount;
		Vector3[] temp = new Vector3[count];
		for (int i = 0; i < count; i++)
			{
			temp[i] = gameObject.transform.GetChild(i).position;
			}
		points = GetCurved(temp);
		for (int a = 0; a < points.Length - 1; a++)
		{

			Gizmos.DrawLine(points[a], points[a + 1]);

		}
	}
	public static void DrawCurved(Vector3[] pathPoints)
		{
		
		pathPoints = GetCurved(pathPoints);
		Vector3 prevPt = pathPoints[0];
		Vector3 currPt;

		for (int i = 1; i < pathPoints.Length; ++i)
			{
			currPt = pathPoints[i];
			Gizmos.DrawLine(currPt, prevPt);
			prevPt = currPt;
			}
		}


	public static Vector3[] GetCurved(Vector3[] waypoints)
		{
		//helper array for curved paths, includes control points for waypoint array
		Vector3[] gizmoPoints = new Vector3[waypoints.Length + 2];
		waypoints.CopyTo(gizmoPoints, 1);
		gizmoPoints[0] = waypoints[1];
		gizmoPoints[gizmoPoints.Length - 1] = gizmoPoints[gizmoPoints.Length - 2];

		Vector3[] drawPs;
		Vector3 currPt;

		//store draw points
		int subdivisions = gizmoPoints.Length * 10;
		drawPs = new Vector3[subdivisions + 1];
		for (int i = 0; i <= subdivisions; ++i)
			{
			float pm = i / (float)subdivisions;
			currPt = GetPoint(gizmoPoints, pm);
			drawPs[i] = currPt;
			}

		return drawPs;
		}



	public static Vector3 GetPoint(Vector3[] gizmoPoints, float t)
		{
		int numSections = gizmoPoints.Length - 3;
		int tSec = (int)Mathf.Floor(t * numSections);
		int currPt = numSections - 1;
		if (currPt > tSec)
			{
			currPt = tSec;
			}
		float u = t * numSections - currPt;

		Vector3 a = gizmoPoints[currPt];
		Vector3 b = gizmoPoints[currPt + 1];
		Vector3 c = gizmoPoints[currPt + 2];
		Vector3 d = gizmoPoints[currPt + 3];

		return .5f * (
			(-a + 3f * b - 3f * c + d) * (u * u * u)
			+ (2f * a - 5f * b + 4f * c - d) * (u * u)
			+ (-a + c) * u
			+ 2f * b
		);
		}
	}
