﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class tragectoryPoint : MonoBehaviour
{


     LineRenderer line;
    public Transform Indicator;
    public Vector3 Offset;
    public Transform SpawnPosition;


    public Material LineRenedererMaterial;
    public Color hitOnCenter;
    public Color hitRegular;

    // Start is called before the first frame update
    void Start()
    {
        line = GetComponent<LineRenderer>();
    }

    // Update is called once per frame
    void Update()
    {
        //   if (GameManager.instance.gameStates==GameManager.GameStates.Running) 
        {
            RaycastHit hit;
            // Does the ray intersect any objects excluding the player layer
            if (Physics.Raycast(transform.position, Vector3.down, out hit, Mathf.Infinity))
            {
                
                Indicator.position = hit.point+ Offset;
                Debug.DrawRay(transform.position, Vector3.down * 110000f, Color.red);

                
                if(hit.transform.tag=="slowmo")
                {
                    LineRenedererMaterial.color = hitOnCenter;
                   // Debug.Log("aisjdiahjdias");
                }
                else
                {
                    LineRenedererMaterial.color = hitRegular;

                }


            }
            else
            {
             //   line.enabled = false;
            }
            line.SetPosition(0, SpawnPosition.position);
            line.SetPosition(1, Indicator.position);
        }
    }
}
