﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OpponentController : Player
{
    // Start is called before the first frame update
    public int counterIntellegence;
    public Transform destination;
    public float thresholdVal;
    public float forwardSpeed;
    public Material[] Cloths;
    public SkinnedMeshRenderer renderer;
    Coroutine move;
    void Start()
    {
        canRotate = true;
        renderer.material = Cloths[Random.Range(0, Cloths.Length)];
    }

    // Update is called once per frame
    void Update()
    {
        if (GameManager.instance.gameState == GameManager.GameState.Start)
        {
            rotate();
        }

    }
    IEnumerator moveForward()
    {
        Debug.Log("aaa");
        while (transform.position.x != destination.transform.position.x && transform.position.z != destination.transform.position.z)
        {

            transform.position = Vector3.MoveTowards(transform.position, new Vector3(destination.position.x, transform.position.y, destination.position.z), forwardSpeed * Time.deltaTime);
            yield return new WaitForEndOfFrame();

        }
    }
    private void OnTriggerEnter(Collider other)
    {
        base.OnTriggerEnter(other);

    }
    private void OnCollisionEnter(Collision collision)
    {
        if (GameManager.instance.gameState == GameManager.GameState.Start)
        {
            base.OnCollisionEnter(collision);
            if (collision.gameObject.tag == "ground")
            {
                //Debug.Log("doundf");
                counterIntellegence++;
                if (counterIntellegence % OpponentManager.instance.setOpponentsPerLevel[GameManager.instance.levelNo].IntellegenceLevel == 0)
                {
                    Debug.Log("Forward");
                    destination = collision.transform.root.GetComponent<plateFormScript>().nextObject;
                    if (move != null)
                    {
                        StopCoroutine(move);

                    }
                    move = StartCoroutine(moveForward());
                    counterIntellegence = 0;
                }


            }
        }
    }

}
