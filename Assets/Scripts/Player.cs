﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    public float speed;
    public Rigidbody rb;
    public Animator PlayerCharAnimator;
    public float globalGravity = 9.81f;
    public float bounciness;
    public float xLimit, yLimit = 2f, zLimit;
    public float BounceMultiplierForBigJump=40;
    public float NormalBounce=12;

    private Vector3 lastVelocity;
    public bool canRotate;
    public bool mouseUp;
    public int playerCurrentPositionOnPlateform;
    public int playerPositition;
    public string playerName;

    public enum PlayerState
    {
        idle,jumping,dead,finished
    };
    public PlayerState playerState;
    // Start is called before the first frame update
    void Start()
    {
        Physics.gravity = new Vector3(0, -globalGravity, 0);
        //bounciness = Mathf.Clamp(bounciness, 0f, 1f);
        rb = GetComponent<Rigidbody>();
    }





    void FixedUpdate()
    {
        lastVelocity = rb.velocity;
    }
    protected void rotate()

    {
        if (target != null && canRotate)
        {
            Vector3 targetDirection = target.position - transform.position;

            // The step size is equal to speed times frame time.
            float singleStep = speed * Time.deltaTime;

            // Rotate the forward vector towards the target direction by one step
            Vector3 newDirection = Vector3.RotateTowards(transform.forward, targetDirection, singleStep, 0.0f);
            newDirection = new Vector3(newDirection.x, 0, newDirection.z);
            //Debug.Log("new dir" + newDirection);
            // Draw a ray pointing at our target in
            // Debug.DrawRay(transform.position, newDirection, Color.red);

            // Calculate a rotation a step closer to the target and applies rotation to this object
            transform.rotation = Quaternion.LookRotation(newDirection);
            //RaycastHit hit;
            //if(Physics.Raycast(transform.position,Vector3.down,out hit,Mathf.Infinity))
            //{
            //    yPos = hit.point.y;
            //   // Vector3 reflectVec = Vector3.Reflect(incomingVec, hit.normal);
            //}
            //else
            //yPos = Mathf.Infinity;
            //indicator.transform.position = new Vector3(transform.position.x,yPos,transform.position.z);

        }
    }

    // Update is called once per frame
    void Update()
    {
       // rotate();
    }
    public Transform target;
    public void ChangeDirection()
    {
        rb.velocity = new Vector3(rb.velocity.z, rb.velocity.y, rb.velocity.x);
    }

    protected void OnCollisionEnter(Collision collision)
    {
        if (playerState != PlayerState.finished)
        {
            if(collision.gameObject.tag=="bigjump")
            {
                PlayerCharAnimator.SetTrigger("jump");
                yLimit = BounceMultiplierForBigJump;
                if (gameObject.tag == "Player")
                {
                    if (collision.gameObject.name == "Plateform")
                        UiManager.instance.ShowMessage("Big Jump");
                }

            }
            else
            {
                yLimit = NormalBounce;
            }
            if(collision.gameObject.tag=="enemy")
            {
                collision.gameObject.GetComponent<Player>().Dead();
                
            }
           

            if (collision.gameObject.tag == "ground")
            {

                iTween.PunchScale(collision.gameObject.gameObject, iTween.Hash("amount", new Vector3(0f, 1f, 0f), "time", 1f, "delay", 0, "easetype", iTween.EaseType.punch, "LoopType", iTween.LoopType.none));
                playerCurrentPositionOnPlateform = int.Parse(collision.transform.root.GetComponent<plateFormScript>().TrampolineNo.text);

                PlayerCharAnimator.SetTrigger("jump");
                if (gameObject.tag == "Player")
                {
                   
                    collision.transform.root.GetComponent<plateFormScript>().circleAnimate();

                    if (collision.gameObject.name == "Plateform")
                        UiManager.instance.ShowMessage("Jump");
                    collision.gameObject.name = collision.gameObject.name.ToString() + "0";
                    // Debug.Log("aa" + (LevelDetails.instance.platefromPoints.Count - playerCurrentPositionOnPlateform) / (float)LevelDetails.instance.platefromPoints.Count);
                    UiManager.instance.FillBar((LevelDetails.instance.platefromPoints.Count - playerCurrentPositionOnPlateform) / (float)LevelDetails.instance.platefromPoints.Count);


                }
                collision.transform.root.GetComponent<plateFormScript>().isHitted = true;

                target = collision.transform.root.GetComponent<plateFormScript>().nextObject;
                if (mouseUp)
                {
                    canRotate = true;
                }



                //if (!collision.transform.gameObject.GetComponent<plateFormScript>().isHitted)
                //{

                //}

            }
            if (collision.gameObject.tag == "finish")
            {
                playerCurrentPositionOnPlateform = int.Parse(collision.transform.root.GetComponent<plateFormScript>().TrampolineNo.text);
                UiManager.instance.FillBar((LevelDetails.instance.platefromPoints.Count - playerCurrentPositionOnPlateform) / (float)LevelDetails.instance.platefromPoints.Count);

                rb.isKinematic = true;
                PlayerCharAnimator.SetTrigger("idle");

                GameManager.instance.winnersList.Add(gameObject);
                playerState = PlayerState.finished;
                if (gameObject.tag != "Player")
                {
                    gameObject.SetActive(false);

                }
                if (gameObject.tag == "Player")
                {
                    GameManager.instance.GameCompleted();

                }

            }
             if (playerState != PlayerState.finished)
            {
                {
                   
                    Vector3 normal = Vector3.zero;
                    foreach (ContactPoint c in collision.contacts)
                    {
                        normal += c.normal;
                    }
                    normal.Normalize();
                    Vector3 inVelocity = lastVelocity;
                    Vector3 outVelocity = bounciness * (-2f * (Vector3.Dot(inVelocity, normal) * normal) + inVelocity);
                    outVelocity = new Vector3(Mathf.Clamp(outVelocity.x, -xLimit, xLimit), Mathf.Clamp(outVelocity.y, yLimit, yLimit), Mathf.Clamp(outVelocity.z, -zLimit, zLimit));
                    rb.velocity = outVelocity;
                    //print(outVelocity+"    ???    ");
                }
            }
           

        }
    }
    protected void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "deadarea")
        {
            Debug.Log("dead");
            playerState = PlayerState.dead;
            GameManager.instance.GameCompleted();
            if(gameObject.tag=="Player")
            {
                UiManager.instance.ShowMessage("Dead");
                
            }
        }
    }
    public void Dead()
    {
        GetComponent<Collider>().enabled = false;
        rb.velocity = Vector3.zero;
        GetComponent<Player>().enabled = false;

    }
}
