﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class plateFormScript : MonoBehaviour
{
    // Start is called before the first frame update
    public bool isHitted;
    public Transform nextObject;
    public TextMesh TrampolineNo;
    public Transform destinationposition;
    public GameObject plateform;
    public GameObject wing;
    public GameObject wing2;
    public GameObject circle;


    void Start()
    {
        int rand = Random.Range(0, 4);
        if (rand == 1)
        {
            iTween.MoveTo(plateform, iTween.Hash("position", destinationposition.transform.position, "time", 2.5f, "easetype", iTween.EaseType.linear, "LoopType", iTween.LoopType.pingPong));
        }
        else
            plateform.transform.localPosition = Vector3.zero;
        iTween.RotateTo(wing, iTween.Hash("rotation", wing. transform.eulerAngles+ new Vector3(0, 0,-60), "time", 2f, "delay", 0f, "EaseType", iTween.EaseType.linear, "LoopType", iTween.LoopType.pingPong));
        iTween.RotateTo(wing2, iTween.Hash("rotation", wing2. transform.eulerAngles+ new Vector3(0, 0,60), "time", 2f, "delay", 0f, "EaseType", iTween.EaseType.linear, "LoopType", iTween.LoopType.pingPong));
    }

    // Update is called once per frame
    void Update()
    {
        ///  iTween.MoveTo()

    }
    public void circleAnimate()
    {
        circle.SetActive(true);
        circle.GetComponent<Animator>().SetTrigger("circle");
    }
}
