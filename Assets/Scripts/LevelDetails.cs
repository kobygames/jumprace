﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelDetails : MonoBehaviour
{
    // Start is called before the first frame update
    public static LevelDetails instance;
    public GameObject plateFormPrefab;
    public Transform PointsParent;
    public List<Transform> platefromPoints = new List<Transform>();

    public List<GameObject> Platforms = new List<GameObject>();
    public LineRenderer lineRenderer;
    public Bezier bezierPoints;
    void Awake()
    {
        instance = this;
    }
    private void Start()
    {
        setAllPoints();
    }
    private void setAllPoints()
    {
        lineRenderer.positionCount=bezierPoints.points.Length;
      //  for (int  i: int = 0; i < pathNodes.length; i++){
            
        
        for (int i=0;i<PointsParent.childCount;i++)
        {
            platefromPoints.Add(PointsParent.GetChild(i));
            GameObject go = Instantiate(plateFormPrefab, PointsParent.GetChild(i).position, PointsParent.GetChild(i).rotation);
           if(i== PointsParent.childCount-1)
            {
                go.transform.GetChild(0).GetChild(0).tag = "finish";
            }
           
            Platforms.Add(go);
            go.GetComponent<plateFormScript>().TrampolineNo.text = (PointsParent.childCount - i).ToString();
            if (i > 0)
            {
                Platforms[i - 1].transform.GetComponent<plateFormScript>().nextObject = go.transform;
               
            }
         //   go.transform.parent = GameManager.instance.trampolineParent;




        }
        for (int i = 0; i < bezierPoints.points.Length; i++)
        {
            lineRenderer.SetPosition(i, bezierPoints.points[i]);
        }
        OpponentManager.instance.INIT();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
