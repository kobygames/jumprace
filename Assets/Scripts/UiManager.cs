﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UiManager : MonoBehaviour
{
    // Start is called before the first frame update
    public static UiManager instance;
    public GameObject menuPanel, gameplayPanel, completePanel;



    public Text [] GamePlay_name;
    public Text CompletePanelMessgae;
   // public Text GamePlay_Sec_name;
    //public Text GamePlay_third_name; 
    public Text [] GamePlaypos;
    public Image PlayerProgressbar;
    public float progressBarSpeed;
   // public Text GamePlay_Sec_pos;
   // public Text GamePlay_third_pos;



    public Text [] GameCompletedPos;
    public Text []GameCompletedName;

    public GameObject messages;



    private void Awake()
    {
        instance = this;
    }
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void StartGame()
    {
        menuPanel.SetActive(false);
        gameplayPanel.SetActive(true);
        GameManager.instance.gameState = GameManager.GameState.Start;
        GameManager.instance.PlayerKinematic(false);
    }
    public void Restart()
    {
        Application.LoadLevel(0);
    }
    public void ShowMessage(string mes)
    {
        messages.GetComponent<Text>().text = mes. ToString();
        messages.SetActive(true);
        Invoke("HideMessages", GameManager.instance.messagesTime);
    }
    public void HideMessages()
    {
        messages.SetActive(false);
    }
    public void FillBar(float val)
    {
        if(barRot!=null)
        {
            StopCoroutine(barRot);
        }
        barRot= StartCoroutine(barFillRoutine(val));
    }
    Coroutine barRot;
    IEnumerator barFillRoutine(float val)
    {
        Debug.Log(val);
        while(PlayerProgressbar.fillAmount!=val)
        {
            Debug.Log("bar");
            yield return new WaitForEndOfFrame();
            PlayerProgressbar.fillAmount = Mathf.MoveTowards(PlayerProgressbar.fillAmount, val, progressBarSpeed * Time.deltaTime);
        }

    }
    

}
