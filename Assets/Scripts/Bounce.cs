using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bounce : MonoBehaviour {

    public float globalGravity=9.81f;
    public float bounciness;
    public float xLimit, yLimit=2f, zLimit; 

    private Vector3 lastVelocity;
    public Rigidbody rb;
    private float collisionAngleTest1;
    private string collisionAngleTest2;

    public void Start()
    {
        Physics.gravity = new Vector3(0,-globalGravity,0);
        //bounciness = Mathf.Clamp(bounciness, 0f, 1f);
        rb = GetComponent<Rigidbody>();
    }
    void FixedUpdate()
    {
        lastVelocity = rb.velocity;
    }
    public void ChangeDirection()
    {
        rb.velocity = new Vector3(rb.velocity.z, rb.velocity.y, rb.velocity.x);
    }
    protected void OnCollisionEnter(Collision collision)
    {
   
        {
          //  if (collision.gameObject.tag == "floor" || collision.gameObject.tag == "wall")
            {
                Vector3 normal = Vector3.zero;
                foreach (ContactPoint c in collision.contacts)
                {
                    normal += c.normal;
                }
                normal.Normalize();
                Vector3 inVelocity = lastVelocity;
                Vector3 outVelocity = bounciness * (-2f * (Vector3.Dot(inVelocity, normal) * normal) + inVelocity);
                outVelocity = new Vector3(Mathf.Clamp(outVelocity.x, -xLimit, xLimit), Mathf.Clamp(outVelocity.y, yLimit, yLimit), Mathf.Clamp(outVelocity.z, -zLimit, zLimit));
                rb.velocity = outVelocity;
                //print(outVelocity+"    ???    ");
            }
        }
        if (collision.gameObject.tag == "ground")
        {
            collision.transform.parent.GetComponent<plateFormScript>().isHitted = true;
            if (!collision.transform.parent.GetComponent<plateFormScript>().isHitted)
            {

            }

        }
        
     
    }
}
