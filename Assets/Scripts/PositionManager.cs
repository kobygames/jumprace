﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
public class PositionManager : MonoBehaviour
{
   
    // Start is called before the first frame update
    public List<playerDetail> playerposition_list = new List<playerDetail>();
    public List<string> topPlayersName = new List<string>();
    
    


    private int pos;
  


 

    // Update is called once per frame
    void Update()
    {
        //if(GameManager.instance.game)
      //  PositionChecking();
      if(GameManager.instance.gameState==GameManager.GameState.Start)
        {
            PositionChecking();
        }
    }
    
    private void PositionChecking()
    {
        playerposition_list.Clear();
        for (int i = 0; i < GameManager.instance. Players.Count; i++)
        {
            playerDetail playerdetail = new playerDetail();
            try
            {
                playerdetail.distance = GameManager.instance.Players[i].GetComponent<Player>().playerCurrentPositionOnPlateform;
            }
            catch(System. NullReferenceException)
            {
                Debug.Log(GameManager.instance.Players[i].name);
            }
            playerdetail.player = GameManager.instance.Players[i];
            playerposition_list.Add(playerdetail);
        }

        var orderbyresult = from s in playerposition_list
                            orderby s.distance
                            select s;
        pos = 0;
        // int posNo = 0;
        topPlayersName.Clear();
        foreach (var std in orderbyresult)
        {
         //  Debug.Log(std.player.name+std.distance);
            std.player.GetComponent<Player>().playerPositition = pos;
            topPlayersName.Add(std.player.name);
          //  SetPlayerPos();
           

            

            pos++;
        }
        SetPlayerPos();
    }

    public void SetPlayerPos()
    {
        
        for(int i=0;i<UiManager.instance.GamePlay_name.Length;i++)
        {
            UiManager.instance.GamePlay_name[i].text = topPlayersName[i];
            UiManager.instance.GamePlaypos[i].text = (i+1)+"";
        }
        if (GameManager.instance.player.GetComponent<Player>().playerPositition > 1)
        {
            UiManager.instance.GamePlay_name[UiManager.instance.GamePlay_name.Length-1].text = "Player";
            UiManager.instance.GamePlaypos[UiManager.instance.GamePlay_name.Length-1].text =
                "" + (GameManager.instance.player.GetComponent<Player>().playerPositition + 1);
        }

    }
    public void PositionSetOnCompleted()
    {
        for (int i = 0; i < UiManager.instance.GameCompletedPos.Length; i++)
        {
            UiManager.instance.GameCompletedName[i].text = topPlayersName[i];
           // UiManager.instance.GamePlaypos[i].text = (i + 1) + "";
        }
        
    }
    
    [System.Serializable]
    public class playerDetail
    {

        public GameObject player;
        public float distance;

    }
}
