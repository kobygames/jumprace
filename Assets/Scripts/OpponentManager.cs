﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OpponentManager : MonoBehaviour
{
    // Start is called before the first frame update
    public static OpponentManager instance;
    

    public List<Opponenets> setOpponentsPerLevel = new List<Opponenets>();
    public GameObject OpponenetPrefab;
    public List<GameObject> opponentInLevel = new List<GameObject>();


    int rand;

    private void Awake()
    {
        instance = this;
    }

    void Start()
    {
        rand = Random.Range(0, GameManager.instance.Names.Length);
    }

    public void INIT()
    {
        SpawnOpponents();
    }
    public void SpawnOpponents()
    {

        for (int i = 0; i < setOpponentsPerLevel[GameManager.instance.levelNo].noOfOpponents; i++)
        {


            Vector3 pos = LevelDetails.instance.platefromPoints[
               setOpponentsPerLevel[GameManager.instance.levelNo].Startingno].position;

            pos += new Vector3(0, 10, 0);
            GameObject go = Instantiate(OpponenetPrefab, pos, Quaternion.identity);
            go.name = GameManager.instance.Names[rand];
            go.GetComponent<Player>().playerName = go.name;
            rand++;
            rand %= GameManager.instance.Names.Length;
            opponentInLevel.Add(go);


            setOpponentsPerLevel[GameManager.instance.levelNo].Startingno += setOpponentsPerLevel[GameManager.instance.levelNo].OpponenetPosIncrement;
        }
        GameManager.instance.INIT();
        

    }
    // Update is called once per frame
    void Update()
    {

    }
    [System.Serializable]
    public class Opponenets
    {
        public int noOfOpponents;
        public int Startingno;
        public int OpponenetPosIncrement;
        [Range(1, 10)]
        public int IntellegenceLevel;

    }
}
