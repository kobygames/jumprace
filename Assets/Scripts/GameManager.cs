﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    // Start is called before the first frame update
    public static GameManager instance;
    public PositionManager positionManager;
    public List<GameObject> winnersList = new List<GameObject>();
    public string[] Names;
    public GameObject player;
    public List<GameObject> Players = new List<GameObject>();
    public int levelNo;
    public float messagesTime;
    public Transform trampolineParent;

    public enum GameState
    {
        Loading,Start,Complete
    };
    public GameState gameState;
    void Awake()
    {
        instance = this;
    }
   public void INIT()
    {
        // Players=
        Players = OpponentManager.instance.opponentInLevel;
        Players.Add(player);
    }
    public void PlayerKinematic(bool val)
    {
        for(int i=0;i<Players.Count;i++)
        {
            Players[i].GetComponent<Rigidbody>().isKinematic = val;
        }
    }
    public void GameCompleted()
    {
        gameState = GameState.Complete;
        positionManager.PositionSetOnCompleted();
        player.GetComponent<PlayerControlller>().Camera.parent = null;
        player.GetComponent<LineRenderer>().enabled = false;

        // PlayerKinematic(true);
        Invoke("showCompletePanel",1.5f);
        player.GetComponent<LineRenderer>().enabled = false;
    }
    public void showCompletePanel()
    {
        if(GameManager.instance.player.GetComponent<Player>().playerPositition<=1)

        {
            UiManager.instance.CompletePanelMessgae.text = "Level Completed!";
        }
        else
            UiManager.instance.CompletePanelMessgae.text = "Level Failed";

        UiManager.instance.completePanel.SetActive(true);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

}
