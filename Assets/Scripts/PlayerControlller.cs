﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[System.Serializable]
public class PlayerControlller : Player {

    public static PlayerControlller Instance;
    public Transform Camera;
    

    #region Public fields
    public float Rotspeed;
    public float forwardSpeed;
    public Transform spawnPosition;
    public float slowmoTime;
    public ParticleSystem HittedPaarticle;
   
 
    public Transform indicator;
    
    public float yOffset;
    #endregion

    #region Private fields
    private float rotY;
    float yPos;
    #endregion



    //  public GameObject Cam;

    void Start()
    {
       
    //    GetComponent<rotateObject>().enabled = true;

     
        Instance = this;
        
    }
    
  
  
    void Update()
    {
        if (GameManager.instance.gameState == GameManager.GameState.Start)
        {
            if (Input.GetMouseButton(0))
            {
                transform.Translate(Vector3.forward * forwardSpeed * Time.deltaTime);
                canRotate = false;
                mouseUp = false;
            }
            if (Input.GetMouseButtonUp(0))
            {

                mouseUp = true;
            }
            rotate();
        }

        

    }
    public void StartSlowmo()
    {
        Time.timeScale = 0.5f;
        Time.fixedDeltaTime = 0.02F * Time.timeScale;
        Invoke("EndSlowmo", slowmoTime * Time.timeScale);
    }
    public void EndSlowmo()
    {
        Time.timeScale = 1;
        Time.fixedDeltaTime = 0.02F;

    }
    private  void OnTriggerEnter(Collider other)
    {
        base.OnTriggerEnter(other);
        if (other.gameObject.tag=="slowmo")
        {
            other.gameObject.SetActive(false);
            StartSlowmo();
            HittedPaarticle.Play();
            // LineRenedererMaterial.color = hitOnCenter;
        }
        
    }
    
    void OnMouseDrag()
{
        if(GameManager.instance.gameState==GameManager.GameState.Start)
        {


            float rotX = Input.GetAxis("Mouse X") * Rotspeed * Mathf.Deg2Rad;
            rotY = Input.GetAxis("Mouse Y") * Rotspeed * Mathf.Deg2Rad;
            transform.RotateAround(Vector3.up, rotX);

         
        }
        

    }
   
   

    private float WrapAngle(float angle)
    {
        angle %= 360;
        if (angle > 180)
            return angle - 360;
        return angle;
        
    }
}
